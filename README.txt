*************
** README: **
*************

DESCRIPTION:
-----------
This module provides a field type for cck with table view of 2 taxonomy categories. Compoound taxonomy values. 


REQUIREMENTS:
-------------
The taxonomy_matrix.module requires the content.module to be installed.


INSTALLATION:
-------------

1. Place the entire taxonomy matrix directory into your Drupal modules/
   directory.


2. Enable the taxonomy matrix module by navigating to:

     administer > modules

   Enabling the taxonomy matrix module will create the necessary database 
   tables for you.


USING THE TAXONOMY MATRIX MODULE:
---------------------------------
After enabling the module, you can create a new field. Choose the Taxonomy Matrix field. 
On the configuration page, choose the taxonomy category that you want to use for the subject (Rows) and the options category (Columns).

Choose the widget type: text, checkbox, radio



Author:
-------
Todd Wallar
todd@fuseiq.com
todd.walar@gmail.com

based on original work in the Matrix module by 

Original Author:
Matthias Hutterer
mh86@drupal.org
m_hutterer@hotmail.com

Current Maintainer:
Aaron Fulton
aaron@webtolife.org